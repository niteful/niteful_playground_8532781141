import 'package:flutter/material.dart';
import 'dart:ui';
import 'settings/settings_carousel.dart';
import 'resources/storage.dart';
import 'insights_form.dart';

class InsightsPage extends StatefulWidget
{
  final Storage storage = Storage();
  
  @override
  _InsightsPageState createState() => new _InsightsPageState();
}

class _InsightsPageState extends State<InsightsPage>
{
    String payload = "";

    @override
    void initState()
    {
      super.initState();
      widget.storage.checkSettings();
    }

  @override
  Widget build(BuildContext context)
  {
    return new Scaffold
    (
      appBar: new AppBar
      (
        elevation: 0.0,
        centerTitle: true,
        backgroundColor: Color.fromRGBO(0, 13, 27, 42),
        title: new Padding(
          child: Image.asset('assets/background-tsp.png', scale: 0.5),
          padding: EdgeInsets.all(8.0),
        ),
        actions: <Widget>
        [
          new IconButton
          (
            onPressed: () {
              Navigator.push(
                context, 
                MaterialPageRoute(builder: (context) => SettingCarousel()),
              );
            },
            icon: new Icon(Icons.settings, color: Colors.white)
          ),
        ],
      ),
      backgroundColor: Color.fromRGBO(0, 13, 27, 42),
      body: Padding(
        padding: EdgeInsets.all(30),
        child: new Column
      (
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>
        [
          new Padding(
                padding: EdgeInsets.all(20),
                child: new Text("Here is your insight for today.",
                  style: TextStyle(color: Colors.white, fontSize: 18)
                ),
              ),
          new Expanded(
            child: new Card(
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: new ListView(
                      padding: EdgeInsets.all(25),
                      children: [
                        new Text("Here is your insight for today.", style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                        )),
                      ]
                    )
                  ),
                  new Padding(
                    padding: EdgeInsets.all(15),
                    child: new FlatButton.icon(
                      icon: Icon(Icons.question_answer, size: 30, color: Color.fromRGBO(0, 13, 27, 42)),
                      label: Text("React", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20, color: Color.fromRGBO(0, 13, 27, 42))
                      ),
                      onPressed: () {
                        Navigator.push(
                          context, 
                          MaterialPageRoute(builder: (context) => InsightsForm()),
                        );
                      },
                    ),
                  )
                ],
              ),
            ),
          ),
          new Container(
            margin: EdgeInsets.all(50),
            child: new FlatButton.icon(
                icon: Icon(Icons.assessment, size: 30, color: Colors.white),
                label: Text("Deep Dive", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white)
                ),
                onPressed: () { 
                   showModalBottomSheet<void>(context: context,
                      builder: (BuildContext context) {
                        return new Scaffold(
                          backgroundColor: Color.fromRGBO(18, 22, 25, 1.0),
                          body: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              new Container(
                                height: 10
                              ),
                              new Padding(
                                padding: EdgeInsets.all(10),
                                child: new ListTile(
                                  leading: new Icon(Icons.bubble_chart, color: Colors.white, size: 35),
                                  title: new Text('Appreciate', style: TextStyle(color: Colors.white)),
                                  onTap: () {
                                    Navigator.pop(context);
                                  },          
                                ),
                              ),
                              new Padding(
                                padding: EdgeInsets.all(10),
                                child: new ListTile(
                                  leading: new Icon(Icons.center_focus_weak, color: Colors.white, size: 35),
                                  title: new Text('Frame', style: TextStyle(color: Colors.white)),
                                  onTap: () {
                                    Navigator.pop(context);
                                  },       
                                ),
                              ),
                              new Padding(
                                padding: EdgeInsets.all(10),
                                child: new ListTile(
                                  leading: new Icon(Icons.recent_actors, color: Colors.white, size: 35),
                                  title: new Text('Reflect', style: TextStyle(color: Colors.white)),
                                  onTap: () {
                                    Navigator.pop(context);
                                  },   
                                ),
                              ),
                              new Padding(
                                padding: EdgeInsets.all(10),
                                child: new ListTile(
                                  leading: new Icon(Icons.info_outline, color: Colors.white, size: 35),
                                  title: new Text('Insights', style: TextStyle(color: Colors.white)),
                                  onTap: () {
                                    Navigator.pop(context);
                                  },   
                                ),
                              ),
                            ]
                          ),
                          floatingActionButton: FloatingActionButton(
                            onPressed: () {},
                            backgroundColor: Colors.white,
                            child: new IconButton
                            (
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              icon: new Icon(Icons.close, color: Color.fromRGBO(0, 13, 27, 42)),
                            ),
                          ),
                          floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
                        );
                      });
                  },
            ),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(width: 3.0, color: Colors.white),
              ),
            ),
          ),
        ],
      ),
      ),
    );
  }  
}
